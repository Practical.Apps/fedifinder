from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.hashers import check_password
from django.utils.dateparse import parse_datetime
from .keydict import inst_alias_rev
from .models import fedi_user
from .views import fix_bio


class FediAuthBackend(BaseBackend):
    def authenticate(self, request, **kwargs) -> fedi_user:
        user = kwargs

        email = user.get('email')
        if not email:  # If there is no email - admin log-in
            email = user.get('username')
            if email:  # If there is a username field proceed with check
                end_user = fedi_user.objects.get(email=user['username'])
                rawpw = user['password']
                chkpw = check_password(rawpw, end_user.password)
                if chkpw:
                    return end_user
                else:  # None may be inappropriate but fedi_user.objects.none() breaks contrib.auth
                    return None  # None does not seem to have any ill affects, works as expected
            else:  # If there is no email or username field, fail
                return fedi_user.objects.none()  # Leaving as is for edge case, potential hacks should crash
        else:  # If there was an email field - standard login
            find_user = fedi_user.objects.filter(email=user['email'])

        if len(find_user) == 0:
            print("BE: User was not found in database. Creating new entry...")
            user['note'] = fix_bio(user['note'])[:2000]  # String html tags and trim to fit max length
            user['created_at'] = parse_datetime(user['created_at'])  # Convert time text to time-object form
            user['url_loc'] = "/find/user/" + inst_alias_rev[user['ref']] + "/" + user['acct']
            fedistring = user['email']  # Create new var for email and remove key
            del user['email']  # Having another email field in kwargs causes error
            new_user = fedi_user.objects.create_user(
                email=fedistring,
                username=user['acct'],
                password=None,
                **user
            )
            return new_user
        else:
            print("BE: User was found in database. Returning...")
            # find_user should be a QuerySet with 1 entry, return entry instead of set
            return list(find_user).pop()

    def get_user(self, user_id):
        try:
            return fedi_user.objects.get(pk=user_id)
        except:
            return None
