from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils.dateparse import parse_datetime


class fedi_manager(BaseUserManager):
    def create_user(self, email, username, password=None, **kwargs):
        print("Got to create_user")
        if not email:
            print("No email error")
            raise ValueError("Error creating user (FUN)")
        if not username:
            print("No username error")
            raise ValueError("Error creating user (JUN)")
        print("create_user data:")
        print("Email:" + email)
        print("Username:" + username)
        for entry in ['avatar', 'header', 'created_at', 'display_name', 'url', 'url_loc', 'note']:
            print(entry + ":" + str(kwargs.get(entry)))
        user = self.model(
            email=email,
            username=username,
            avatar=kwargs.get('avatar'),
            bio=kwargs.get('note'),
            header=kwargs.get('header'),
            join_fedi=kwargs.get('created_at'),
            uhand=kwargs.get('display_name'),
            uhost=kwargs.get('ref'),
            url_src=kwargs.get('url'),
            url_loc=kwargs.get('url_loc'),
            xyz="Y"
        )
        print("Create_user - self.model set")
        user.set_password(password)
        print("Create_user - password set")
        user.save(using=self.db)
        print("Create_user - user.save happened")
        return user

    def create_superuser(self, email, username, password, **kwargs):
        user = self.create_user(
            email=email,
            username=username,
            password=password,
            avatar='avatar',
            bio='note',
            header='header',
            created_at=parse_datetime("2021-06-01T00:00:00.000Z"),
            display_name='display_name',
            ref='ref',
            url_loc='url_l',
            url='url_s'
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self.db)
        return user


class fedi_user(AbstractBaseUser):

    class Genders(models.IntegerChoices):
        NOTH = 0
        MALE = 1
        FEMA = 2
        TRNM = 3
        TRNF = 4
        ENBY = 5

    class ISO(models.IntegerChoices):
        BTH = 0
        SOC = 1
        ROM = 2

    class Orient(models.IntegerChoices):
        NTH = 0
        HET = 1
        HOM = 2
        OTH = 3

    class RelStat(models.IntegerChoices):
        NTH = 0
        SNG = 1
        INR = 2
        MAR = 3
        DIV = 4
        WTM = 5

    objects = fedi_manager()
    age = models.IntegerField(default=0)                                 # Age (yr)
    attract = models.CharField(max_length=50, blank=True, null=True)      # Attracted to ______
    avatar = models.CharField(max_length=300)                             # Link to avatar image *
    bio = models.CharField(max_length=2000, blank=True, null=True)        # Bio text *
    email = models.EmailField(max_length=150, unique=True)                # Full handle (ABC@MNO.XYZ) *
    header = models.CharField(max_length=300)                             # Link to header image *
    height_ft = models.IntegerField(default=0)                            # Height (ft)
    height_in = models.IntegerField(default=0)                            # Height (in)
    interests = models.CharField(max_length=100, blank=True, null=True)   # Interest list string
    is_active = models.BooleanField(default=True)                         # Active status (REQ)
    is_admin = models.BooleanField(default=False)                         # Admin status (REQ)
    is_staff = models.BooleanField(default=False)                         # Staff status (REQ)
    is_superuser = models.BooleanField(default=False)                     # Superuser status (REQ)
    join_fedi = models.DateTimeField()                                    # Time user made account *
    join_match = models.DateTimeField(auto_now_add=True)                  # Time user made profile
    last_login = models.DateTimeField(auto_now=True)                      # Time user was last active
    lang_known = models.CharField(max_length=100, blank=True, null=True)  # Languages user knows
    lang_learn = models.CharField(max_length=100, blank=True, null=True)  # Languages user is learning
    location = models.CharField(max_length=50, blank=True, null=True)     # Location
    loc_lat = models.DecimalField(decimal_places=12, max_digits=14, default=0.000000000000)  # Location CoOrd. (Latitude)
    loc_lon = models.DecimalField(decimal_places=12, max_digits=15, default=0.000000000000)  # Location CoOrd. (Longitude)
    mbti = models.CharField(max_length=4, blank=True, null=True)         # Myers–Briggs Type Indicator
    occupation = models.CharField(max_length=50, blank=True, null=True)  # Occupation
    orient = models.IntegerField(choices=Orient.choices, default=0)      # Sexual orientation
    photos = models.CharField(max_length=2000, blank=True, null=True)    # Photo set list
    pol_soc = models.DecimalField(decimal_places=2, max_digits=4, default=0)  # Politics axis (social)
    pol_eco = models.DecimalField(decimal_places=2, max_digits=4, default=0)  # Politics axis (economic)
    quiz = models.CharField(max_length=2000, blank=True, null=True)      # Questionnaire
    race = models.CharField(max_length=50, blank=True, null=True)        # Race
    reli = models.CharField(max_length=50, blank=True, null=True)        # Religion
    rel_stat = models.IntegerField(choices=RelStat.choices, default=0)   # Relationship status
    report = models.CharField(max_length=100, blank=True, null=True)     # Report status
    seek = models.IntegerField(choices=ISO.choices, default=0)           # User is seeking
    sex = models.IntegerField(choices=Genders.choices, default=0)        # Gender
    uhand = models.CharField(max_length=75)                              # Display name *
    uhost = models.CharField(max_length=75)                              # Instance name (abc@MNO.XYZ)*
    username = models.CharField(max_length=75)                           # Username (ABC@mno.xyz) *
    url_loc = models.CharField(max_length=100, unique=True)              # Link to user profile on site
    url_src = models.CharField(max_length=150, unique=True)              # Link to user profile on instance *
    weight = models.IntegerField(default=0)                              # Weight (lb)
    xyz = models.CharField(max_length=150, blank=True, null=True)        # Misc info

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True
