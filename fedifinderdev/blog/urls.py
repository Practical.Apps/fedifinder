from django.urls import path
from . import views

urlpatterns = [
    path('about/', views.about, name='blog-about'),
    path('blog/', views.blog, name='blog-home'),
    path('', views.splash, name='splash'),
]
